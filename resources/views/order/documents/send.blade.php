@component('mail::message')
# Hola, adjuntamos los documentos de tu orden.

Hola, aquí adjuntamos la orden de compra y la cotización correspondiente a tu orden de compra.

@component('mail::button', ['url' => env('APP_URL') . 'storage/' . $order])
Orden
@endcomponent

@component('mail::button', ['url' => env('APP_URL') . 'storage/' . $quotation])
Cotización
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
