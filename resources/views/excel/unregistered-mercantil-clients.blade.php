<html>
    <link rel="stylesheet" href="{{ asset('/css/table.css') }}">
    <!-- Cell styled with class -->
    <table>
        <tr>
            <th class="cell">Nombre del comercio</th>
            <th class="cell">Persona</th>
            <th class="cell">Razón social</th>
            <th class="cell">Persona de contacto</th>
            <th class="cell">Teléfono</th>
            <th class="cell">Correo</th>
            <th class="cell">Región</th>
            <th class="cell">Observación</th>
        </tr>
        <!-- Cell styled with ID -->
        @foreach ($unregisteredMercantilClients as $unregisteredMercantilClient)
            <tr>
                <td class="cell">{{ $unregisteredMercantilClient->commerce_name }}</td>
                <td class="cell">{{ $unregisteredMercantilClient->person }}</td>
                <td class="cell">{{ $unregisteredMercantilClient->social_reason }}</td>
                <td class="cell">{{ $unregisteredMercantilClient->contact_person }}</td>
                <td class="cell">{{ $unregisteredMercantilClient->phone }}</td>
                <td class="cell">{{ $unregisteredMercantilClient->email }}</td>
                <td class="cell">{{ $unregisteredMercantilClient->region }}</td>
                <td class="cell">{{ $unregisteredMercantilClient->observation }}</td>
            </tr>
        @endforeach
    </table>
</html>