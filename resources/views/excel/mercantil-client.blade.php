<html>
    <link rel="stylesheet" href="{{ asset('/css/table.css') }}">
    <!-- Cell styled with class -->
    <table>
        <tr>
            <th class="cell">Nombre del comercio</th>
            <th class="cell">Razón social</th>
            <th class="cell">Dato identificador</th>
            <th class="cell">Número de identificación</th>
            <th class="cell">Persona de contacto</th>
            <th class="cell">Teléfono</th>
            <th class="cell">Correo</th>
            <th class="cell">Dirección</th>
            <th class="cell">Pilotos aprobados por segmento</th>
            <th class="cell">Cantidad de mPOS</th>
            <th class="cell">Fecha de contacto</th>
            <th class="cell">Región</th>
            <th class="cell">Especialista gestión</th>
            <th class="cell">Resultado contacto telefónico por MBU</th>
            <th class="cell">Fecha contacto: GP <=> COMERCIO</th>
            <th class="cell">Resultado del contacto (Y/N)</th>
            <th class="cell">Observaciones del Contacto</th>
            <th class="cell">Monto de la cotizacion</th>
            <th class="cell">Número de orden de compra</th>
            <th class="cell">Envio de facturas</th>
            <th class="cell">Fecha de confirmación de pago del cliente</th>
            <th class="cell">Fecha de despacho</th>
            <th class="cell">Fecha recibido por el cliente</th>
            <th class="cell">Observaciones generales del proceso</th>
            <th class="cell">Estatus</th>
        </tr>
        <!-- Cell styled with ID -->
        @foreach ($mercantilClients as $mercantilClient)
            <tr>
                <td class="cell">{{ $mercantilClient->commerce_name }}</td>
                <td class="cell">{{ $mercantilClient->social_reason }}</td>
                <td class="cell">{{ $mercantilClient->identifier_data }}</td>
                <td class="cell">{{ $mercantilClient->identification_number }}</td>
                <td class="cell">{{ $mercantilClient->contact_person }}</td>
                <td class="cell">{{ $mercantilClient->phone }}</td>
                <td class="cell">{{ $mercantilClient->email }}</td>
                <td class="cell">{{ $mercantilClient->address }}</td>
                <td class="cell">{{ $mercantilClient->approved_by_segment }}</td>
                <td class="cell">{{ $mercantilClient->mpos_quantity }}</td>
                <td class="cell">{{ date('d-m-Y', strtotime($mercantilClient->date_of_contact)) }}</td>
                <td class="cell">{{ $mercantilClient->region }}</td>
                <td class="cell">{{ $mercantilClient->mbu_management_specialist }}</td>
                <td class="cell"></td>
                <td class="cell"></td>
                <td class="cell"></td>
                <td class="cell"></td>
                <td class="cell">{{ $mercantilClient->quotation_amount }}</td>
                <td class="cell">{{ $mercantilClient->order_number }}</td>
                <td class="cell"></td>
                <td class="cell">
                    {{ ($mercantilClient->payment_confirmation_date) ? date('d-m-Y', strtotime($mercantilClient->payment_confirmation_date)) : 'No se ha confirmado el pago' }}
                </td>
                <td class="cell">
                    {{ ($mercantilClient->dispatch_date) ? date('d-m-Y', strtotime($mercantilClient->dispatch_date)) : 'No se ha despachado la orden' }}
                </td>
                <td class="cell">
                    {{ ($mercantilClient->received_client_date) ? date('d-m-Y', strtotime($mercantilClient->received_client_date)) : 'El cliente no lo ha recibido' }}
                </td>
                <td class="cell">Observaciones generales del proceso</td>
                <td class="cell">{{ ($mercantilClient->status == 'valid' ? 'Valida' : 'Expirada') }}</td>
            </tr>
        @endforeach
    </table>
</html>