<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


	Route::prefix('orders')->group(function() {
		Route::get('/', 'OrderController@index')->name('orders.index');
		Route::get('/phase', 'OrderController@phase')->name('orders.phase');
		Route::get('/{id}', 'OrderController@show')->name('orders.show');
		Route::get('/{id}/traking', 'OrderController@traking')->name('orders.traking');
		Route::get('/{id}/documents', 'OrderController@documents')->name('orders.documents');
		Route::post('/create', 'OrderController@store')->name('orders.store');
		Route::put('/edit/{id}', 'OrderController@update')->name('orders.update');
		Route::delete('/delete/{id}', 'OrderController@destroy')->name('orders.delete');

		Route::put('/change-status/{id}', 'OrderController@changeStatus')->name('orders.change-status');
		Route::put('/change-phase/{id}', 'OrderController@changePhase')->name('orders.change-phase');
		Route::put('/update-dispatch-date/{id}', 'OrderController@updateDispatchDate')->name('orders.change-dispatch-date');
		Route::put('/update-received-client-date/{id}', 'OrderController@updateReceivedClientDate')->name('orders.change-received-client-date');
		Route::post('/{id}/send-order-documents', 'OrderController@sendOrderDocuments')->name('orders.send-order-documents');
	});

	Route::prefix('mercantil-clients')->group(function() {
		Route::get('/', 'MercantilClientController@index')->name('mercantil-clients.index');
		Route::post('/import', 'MercantilClientController@import')->name('mercantil-clients.import');
		Route::get('/export', 'MercantilClientController@export')->name('mercantil-clients.export');
		Route::get('/rif', 'MercantilClientController@getByRif')->name('mercantil-clients.get-by-rif');
		Route::get('/{id}', 'MercantilClientController@show')->name('mercantil-clients.show');
		Route::post('/create', 'MercantilClientController@store')->name('mercantil-clients.store');
		Route::get('/{id}/extra-data', 'MercantilClientController@getExtraData')->name('mercantil-clients.extra-data');
		Route::put('/edit/{id}', 'MercantilClientController@update')->name('mercantil-clients.update');
		Route::delete('/delete/{id}', 'MercantilClientController@destroy')->name('mercantil-clients.delete');
	});

	Route::prefix('unregistered-mercantil-clients')->group(function() {
		Route::get('/', 'UnregisteredMercantilClientController@index')->name('unregistered-mercantil-clients.index');
		Route::get('/export', 'UnregisteredMercantilClientController@export')->name('unregistered-mercantil-clients.export');
		Route::get('/rif', 'UnregisteredMercantilClientController@getByRif')->name('unregistered-mercantil-clients.get-by-rif');
		Route::get('/{id}', 'UnregisteredMercantilClientController@show')->name('unregistered-mercantil-clients.show');
		Route::post('/create', 'UnregisteredMercantilClientController@store')->name('unregistered-mercantil-clients.store');
		Route::post('/create-with-call', 'UnregisteredMercantilClientController@storeWithCall')->name('unregistered-mercantil-clients.create-with-call');
		Route::put('/edit/{id}', 'UnregisteredMercantilClientController@update')->name('unregistered-mercantil-clients.update');
		Route::delete('/delete/{id}', 'UnregisteredMercantilClientController@destroy')->name('unregistered-mercantil-clients.delete');
	});

	Route::prefix('extra-data-mercantil-clients')->group(function() {
		Route::get('/', 'ExtraDataMercantilClientController@index')->name('extra-data-mercantil-clients.index');
		Route::get('/{id}', 'ExtraDataMercantilClientController@show')->name('extra-data-mercantil-clients.show');
		Route::post('/create', 'ExtraDataMercantilClientController@store')->name('extra-data-mercantil-clients.store');
		Route::put('/edit/{id}', 'ExtraDataMercantilClientController@update')->name('extra-data-mercantil-clients.update');
		Route::delete('/delete/{id}', 'ExtraDataMercantilClientController@destroy')->name('extra-data-mercantil-clients.delete');
	});

	Route::prefix('sessions')->group(function() {
		Route::get('/', 'SessionController@index')->name('sessions.index');
		Route::get('/{id}', 'SessionController@show')->name('sessions.show');
		Route::post('/create', 'SessionController@store')->name('sessions.store');
		Route::put('/edit/{id}', 'SessionController@update')->name('sessions.update');
		Route::delete('/delete/{id}', 'SessionController@destroy')->name('sessions.delete');
	});

	Route::prefix('calls')->group(function() {
		Route::get('/', 'CallController@index')->name('calls.index');
		Route::get('/day', 'CallController@getByDay')->name('calls.get-by-day');
		Route::get('/month', 'CallController@getByMonth')->name('calls.get-by-month');
		Route::get('/year', 'CallController@getByYear')->name('calls.get-by-year');
		Route::get('/rif', 'CallController@getByRif')->name('calls.get-by-rif');
		Route::get('/charts', 'CallController@charts')->name('calls.charts');
		Route::get('/{id}', 'CallController@show')->name('calls.show');
		Route::post('/create', 'CallController@store')->name('calls.store');
		Route::put('/edit/{id}', 'CallController@update')->name('calls.update');
		Route::delete('/delete/{id}', 'CallController@destroy')->name('calls.delete');
	});

	Route::prefix('users')->group(function() {
		Route::get('/', 'UserController@index')->name('users.index');
	    Route::get('/data', 'Auth\ApiAuthController@getAuthUser')->name('users.data');
		Route::get('/{id}', 'UserController@show')->name('users.show');
		Route::put('edit/{id}', 'UserController@update')->name('users.update');
		Route::post('/create', 'UserController@store')->name('users.store');
	    Route::delete('/{id}', 'UserController@destroy')->name('users.delete');
	});

	Route::prefix('documents')->group(function() {
		// Route::get('/', 'DocumentController@index')->name('documents.index');
		// Route::get('/{id}', 'DocumentController@show')->name('documents.show');
		Route::post('/create', 'DocumentController@store')->name('documents.store');
		// Route::put('/edit/{id}', 'DocumentController@update')->name('documents.update');
		// Route::delete('/delete/{id}', 'DocumentController@destroy')->name('documents.delete');
	});


Route::post('/login', 'Auth\ApiAuthController@login')->name('users.login');