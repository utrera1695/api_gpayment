##Pasos para descargar el proyecto.

###Requisitos:
Servidor Apache + MySQL + PHP +7.0
Composer
Git
Recomendado: [XAMPP](https://www.apachefriends.org/es/index.html)

1. git clone [Repositorio](https://bitbucket.org/adrianjvera21/gpayment)
2. cd gpayment
3. composer install
4. Crear una base de datos con el nombre y las credenciales de su preferencia
5. Crear en la raíz del proyecto un archivo con el nombre .env siguiendo la misma estructura del archivo .env-example y ahí colocar las credenciales de la BD creada en el paso anterior
6. Ejecutar en la cónsola: php artisan key:generate
7. Ejecutar en la cónsola: php artisan migrate
8. Ejecutar en la cónsola: php artisan db:seed
9. Ejecutar en la cónsola: php artisan serve --port=8080

###RUTAS

Entidad: **Ordenes**

* Retorna todas las ordenes

| GET      | api/orders |

* Retorna una orden en concreto pasada su id 

| GET      | api/orders/{id}

* Cambia la fase de una orden (Requiere cambios)

| PUT      | api/orders/change-stage/{id}
```json
{String: state}
```

* Actualiza la fecha de despacho de una orden
| PUT      | api/orders/update-dispatch-date/{id}
```json
{Date:YYYY-MM-DD: dispatch_date}
```

* Actualiza la fecha en la cual fue recibido una orden
| PUT      | api/orders/update-received-client-date/{id}
```json
{Date:YYYY-MM-DD: received_client_date}
```


* Cambia el estado de una orden (Requiere cambios)

| PUT      | api/orders/change-status/{id}

* Retorna las ordenes de una fase indicada
|GET       | api/orders/phase?&phase=[dispatch, billing, activation]

* Crea una orden

| POST     | api/orders/create
```json
{
    Integer: seller,
    Integer: mercantil_client_id
}
```

* Borra una orden en concreto pasadso osu id

| DELETE   | api/orders/delete/{id}

* Actualiza una orden en concreto pasado su id

| PUT      | api/orders/edit/{id}
```json
{
    Integer: seller
}
```

* Retorna los documentos de una orden de compra

| GET     | api/orders/{id}/documents

* Guarda los documentos de una orden 

| POST      | api/orders/{id}/send-order-documents
```json
{
    Input[file]: order
    Input[file]: quotation
}
```

* Verifica el la fase y el indicador de una orden
| GET       | api/orders/{id}/traking


Entidad: **Clientes de Mercantil**

* Retorna todos los clientes de mercantil

| GET      | api/mercantil-clients

* Retorna un cliente en concreto pasado su id

| GET      | api/mercantil-clients/{id}

* Retorna un cliente por su rif
| GET      | api/mercantil-clients/rif?&rif=

* Retorna todos los datos extras que tiene un cliente
| GET | api/mercantil-clients/{id}/extra-data

* Crea un cliente de mercantil

| POST     | api/mercantil-clients/create
```json
{
    String: commerce_name
    String: person
    String: social_reason
    String: identifier_data
    String: identification_number
    String: contact_person
    String: phone
    String: email
    Integer: approved_by_segment
    Integer: mpos_quantity
    Date: date_of_contact
    String: region
    String: observation
}
```

* Borra un cliente pasado su id

| DELETE   | api/mercantil-clients/delete/{id}

* Actualiza un cliente pasado su id y los datos

| PUT      | api/mercantil-clients/edit/{id}
```json
{
    String: commerce_name
    String: person
    String: social_reason
    String: identifier_data
    String: identification_number
    String: contact_person
    String: phone
    String: email
    Integer: approved_by_segment
    Integer: mpos_quantity
    Date: date_of_contact
    String: region
    String: observation
}
```


Entidad: **Usuarios**

* Devuelve todos los usuarios
|GET        | api/users

* Regresa un usuario
| GET       | api/users/{id}

* Crea un usuario
| POST      | api/users/create
```json
{
    name
    surname
    email
    dni
    role: root, public_attention, activation, billing, dispatch, validation
    password
}
```

* Edita un usuario
| POST      | api/users/edit/{id}
```json
{
    name
    surname
    email
    dni
    role: root, public_attention, activation, billing, dispatch, validation
    password
}
```

* Regresa la información de un usuario logeado pasado el token
|GET        | api/users/data?&token

* Elimina un usuario pasado un id
|DELETE     | api/users/{id}

* Inicia sesión
| POST      | api/login
```json
{
    email,
    password
}
```

Entidad **Documentos**

* Sube los documentos de una orden
|POST       | api/documents/create
{
    Input[file]: documents
    order_id
}

Entidad **Llamadas**

* Todas las llamadas
| GET       | api/calls

* Estadísticas de llamadas
| GET       | api/calls/charts

* Crear una llamada (De un usuario ya creado)
| POST      | api/calls/create
```json
{
    mercantil_client_id
}
```

* Las llamadas de un día
| GET       | api/calls/day?&day=DD-MM-YYYY
* Las llamadas de un mes
| GET       | api/calls/month?&month=M

* LLamadas de un año
| GET       | api/calls/year?&year=YYYY

* Borrar una llamada
| DELETE    | api/calls/delete/{id}

* Llamadas de un cliente por su rif
| GET       | api/calls/rif?&rif

* Una llamada en concreto por su id
| GET       | api/calls/{id}

Entidad **Clientes no registrados**

* Mostrar todos los clientes
| GET      | api/unregistered-mercantil-clients

* Crear un cliente no registrado
| POST     | api/unregistered-mercantil-clients/create
```json
{
    commerce_name
    person
    social_reason
    contact_person
    phone
    email
    region
    observation
}
```

* Crear un cliente no registrado y guarda una llamada automáticamente
| POST     | api/unregistered-mercantil-clients/create-with-call
```json
{
    commerce_name
    person
    social_reason
    contact_person
    phone
    email
    region
    observation
}
```

* Borrar un cliente
| DELETE   | api/unregistered-mercantil-clients/delete/{id}

* Actualizar un cliente
| PUT      | api/unregistered-mercantil-clients/edit/{id}
```json
{
    commerce_name
    person
    social_reason
    contact_person
    phone
    email
    region
    observation
}
```

* Exportar al excel
| GET      | api/unregistered-mercantil-clients/excel

* Retornar un cliente por rif
| GET      | api/unregistered-mercantil-clients/rif

* Retornar un cliente por su id
| GET      | api/unregistered-mercantil-clients/{id}

Entidad: **Datos extra de clientes mercantil**

* Obtiene todos los datos extras
| GET      | api/extra-data-mercantil-clients

* Crea un dato extra
```json
{
    attribute: commerce_name, social_reason, identifier_data, identification_number, contact_person, phone, email, address, mpos_quantity, approved_by_segment, date_of_contact, region, mbu_management_specialist, result_telephone_contact_by_mbu, mpos_quantity_availables
    value: ,
    mercantil_client_id
}
```
| POST     | api/extra-data-mercantil-clients/create

* Borra un dato extra
| DELETE   | api/extra-data-mercantil-clients/delete/{id}

* Actualiza un dato extra
| PUT      | api/extra-data-mercantil-clients/edit/{id}

```json
{
    attribute: commerce_name, social_reason, identifier_data, identification_number, contact_person, phone, email, address, mpos_quantity, approved_by_segment, date_of_contact, region, mbu_management_specialist, result_telephone_contact_by_mbu, mpos_quantity_availables
    value: ,
    mercantil_client_id
}
```

* Obtiene un dato extra
| GET      | api/extra-data-mercantil-clients/{id}

Entidad **Sesiones**

* Obtiene todas las sesiones
GET      | api/sessions

* Obtiene una sesión por su id
GET      | api/sessions/{id}

* Elimina una sesión
DELETE   | api/sessions/delete/{id}