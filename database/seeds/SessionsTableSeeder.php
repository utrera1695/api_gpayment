<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class SessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=0; $i < 80; $i++) { 
        	DB::table('sessions')->insert([
        		'action' => $faker->text($maxNbChars = 80),
        		'date_action' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        		'order_id' => $faker->numberBetween($min = 1, $max = 15),
        		'user_id' => $faker->numberBetween($min = 3, $max = 20)
        	]);
        }
    }
}
