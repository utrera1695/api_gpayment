<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class MercantilClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $prefixPhones = ['0414', '0424', '0416', '0426', '0412'];
        $regions = ['Ciudad Guayana', 'Barcelona', 'Maturín', 'Ciudad Bolívar', 'Cumaná', 'San Cristóbal', 'Barinas', 'Cabimas', 'Punto Fijo', 'Puerto La Cruz', 'Guarenas', 'Los Teques', 'Mérida'];
        for ($i=0; $i < 30; $i++) { 
        	DB::table('mercantil_clients')->insert([
        		'commerce_name' => $faker->company,
        		'person' => $contactPerson = $faker->firstName('male'|'female') . ' ' . $faker->lastName,
        		'social_reason' => 'J-' . $faker->randomNumber($nbDigits = 8, $strict = false),
        		'identifier_data' => $faker->randomNumber($nbDigits = 8, $strict = false),
        		'identification_number' => $faker->randomNumber($nbDigits = 8, $strict = false),
        		'contact_person' => $contactPerson,
        		'phone' => $faker->randomElement($prefixPhones) . '-' .$faker->randomNumber($nbDigits = 7, $strict = false),
        		'email' => formatterEmail($contactPerson) . '@gmail.com',
        		'approved_by_segment' => $faker->randomNumber($nbDigits = 3, $strict = false),
        		'mpos_quantity' => 0,
        		'date_of_contact' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        		'region' => $faker->randomElement($regions),
        		'observation' => $faker->text($maxNbChars = 80),
                'mpos_quantity_availables' => 1
        	]);
        }
    }
}
