<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ExtraDataMercantilClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $attributes = ['commerce_name', 'social_reason', 'identifier_data', 'identification_number', 'contact_person', 'phone', 'email', 'address', 'region', 'mbu_management_specialist', 'result_telephone_contact_by_mbu'];

        for ($i=0; $i < 80; $i++) { 
        	DB::table('extra_data_mercantil_clients')->insert([
        		'attribute' => $faker->randomElement($attributes),
        		'value' => $faker->text($maxNbChars = 15),
                'mercantil_client_id' => $faker->numberBetween($min = 1, $max = 60),
        	]);
        }
    }
}