<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class UnregisteredMercantilClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $prefixPhones = ['0414', '0424', '0416', '0426', '0412'];
        $regions = ['Ciudad Guayana', 'Barcelona', 'Maturín', 'Ciudad Bolívar', 'Cumaná', 'San Cristóbal', 'Barinas', 'Cabimas', 'Punto Fijo', 'Puerto La Cruz', 'Guarenas', 'Los Teques', 'Mérida'];
        for ($i=0; $i < 30; $i++) { 
        	DB::table('unregistered_mercantil_clients')->insert([
        		'commerce_name' => $faker->company,
        		'person' => $contactPerson = $faker->firstName('male'|'female') . ' ' . $faker->lastName,
        		'social_reason' => 'J-' . $faker->randomNumber($nbDigits = 8, $strict = false),
        		'contact_person' => $contactPerson,
        		'phone' => $faker->randomElement($prefixPhones) . '-' .$faker->randomNumber($nbDigits = 7, $strict = false),
        		'email' => formatterEmail($contactPerson) . '@gmail.com',
        		'region' => $faker->randomElement($regions),
        		'observation' => $faker->text($maxNbChars = 80)
        	]);
        }
    }
}
