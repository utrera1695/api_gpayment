<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $phasesAndStages = [
            'billing' => [
                'Factura solicitada',
                'En espera de soporte de pago',
                'En proceso de Conciliacion',
                'En proceso de despacho',
            ],
            'dispatch' => [
                'En bandeja de Salida',
                'En pocesion del courier',
                'En transito a destino',
                'Recibido por el cliente',
            ],
            'activation' => [
                'Activación en proceso',
                'Recibido por el departamento de activación',
                'Activación exitosa',
                'Activación no pudo ser procesada',
            ]
        ];

        for ($i=0; $i < 20; $i++) { 
        	DB::table('orders')->insert([
        		'quotation_number' => $faker->randomNumber($nbDigits = 5, $strict = true),
                'quotation_amount' => $faker->randomFloat($nbMaxDecimals = 3, $min = 30, $max = 80),
                'date' => $date = $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        		'order_number' => $faker->randomNumber($nbDigits = 8, $strict = true),
        		'status' => $faker->randomElement($array = array('valid', 'expired')),
        		'phase' => $phase = $faker->randomElement($array = array('billing', 'dispatch', 'activation')),
        		'seller' => $faker->numberBetween($min = 73, $max = 103),
                'stage' => $faker->randomElement($phasesAndStages[$phase]),
                // 'mercantil_client_id' => $faker->numberBetween($min = 1, $max = 30),
                'mercantil_client_id' => $i + 1,
                'due_date' => $faker->dateTimeInInterval($date, $interval = '+5 day'),
        	]);
        }
    }
}
