<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CallsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        // Llamadas de clientes de mercantil registrados
        for ($i=0; $i < 100; $i++) { 
        	DB::table('calls')->insert([
        		'date' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now'),
        		'mercantil_client_id' => $faker->numberBetween($min = 1, $max = 30),
        		'unregistered_mercantil_client_id' => null
        	]);
        }
    }
}
