<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Generator;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = new Generator();
        $email = '@gpayment.net';

        $faker->addProvider(new \Faker\Provider\es_ES\Person($faker));

        DB::table('users')->insert([
        	'name' => 'Adrián',
			'surname' => 'Vera',
			'email' => 'adrian' . $email,
			'role' => 'root',
			'password' => Hash::make('123456')
        ]);

        DB::table('users')->insert([
        	'name' => 'Victor',
			'surname' => 'Utrera',
			'email' => 'victor' . $email,
			'role' => 'root',
			'password' => Hash::make('123456')
        ]);

        for ($i=0; $i < 15; $i++) {
        	DB::table('users')->insert([
        		'name' => $name = $faker->firstName,
				'surname' => $surname = $faker->lastName,
				'email' => formatterEmail($name . $surname . $email),
				'role' => $faker->randomElement($array = array('public_attention', 'activation', 'billing', 'dispatch', 'validation')),
				'password' => Hash::make('123456')
        	]);
        }

        for ($i=0; $i < 10; $i++) {
            DB::table('users')->insert([
                'name' => $name = $faker->firstName,
                'surname' => $surname = $faker->lastName,
                'email' => formatterEmail($name . $surname . $email),
                'role' => 'public_attention',
                'password' => Hash::make('123456')
            ]);
        }
    }
}

