<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('quotation_number');
            $table->double('quotation_amount');
            $table->date('date');
            $table->string('order_number');
            $table->enum('status', ['valid', 'expired']);
            $table->enum('phase', ['billing', 'dispatch', 'activation']);
            $table->enum('stage', ['En espera de soporte de pago','En proceso de Conciliacion','En proceso de despacho',
                                    'En bandeja de Salida', 'En pocesion del courier', 'En transito a destino',
                                    'Recibido por el cliente', 'Recibido por el departamento de activación', 'Activación exitosa',
                                    'Activación no pudo ser procesada', 'Activación en proceso', 'Factura solicitada']);
            $table->integer('seller')->unsigned();
            $table->date('due_date');
            $table->date('payment_confirmation_date')->nullable();
            $table->date('dispatch_date')->nullable();
            $table->date('received_client_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
