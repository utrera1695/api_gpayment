<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnregisteredMercantilClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unregistered_mercantil_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('commerce_name');
            $table->string('person');
            $table->string('social_reason');
            $table->string('contact_person');
            $table->string('phone');
            $table->string('email');
            $table->string('observation');
            $table->string('region');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unregistered_mercantil_clients');
    }
}
