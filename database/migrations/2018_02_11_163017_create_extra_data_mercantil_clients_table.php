<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraDataMercantilClientsTable extends Migration
{
    private $attributes = ['commerce_name', 'social_reason', 'identifier_data', 'identification_number', 'contact_person', 'phone', 'email', 'address', 'mpos_quantity', 'approved_by_segment', 'date_of_contact', 'region', 'mbu_management_specialist', 'result_telephone_contact_by_mbu', 'mpos_quantity_availables'];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('extra_data_mercantil_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('attribute', $this->attributes);
            $table->string('value');

            $table->integer('mercantil_client_id')->unsigned();
            $table->foreign('mercantil_client_id')->references('id')->on('mercantil_clients')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('extra_data_mercantil_clients');
    }
}
