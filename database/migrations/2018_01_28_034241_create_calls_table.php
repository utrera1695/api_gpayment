<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('date');
            $table->integer('mercantil_client_id')->unsigned()->nullable();
            $table->integer('unregistered_mercantil_client_id')->unsigned()->nullable();

            $table->foreign('mercantil_client_id')->references('id')->on('mercantil_clients')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('unregistered_mercantil_client_id')->references('id')->on('unregistered_mercantil_clients')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
