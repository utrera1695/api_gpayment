<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMercantilClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mercantil_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('commerce_name');
            $table->string('social_reason');
            $table->string('identifier_data');
            $table->string('identification_number');
            $table->string('contact_person');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            // Other data
            $table->integer('mpos_quantity');
            $table->integer('approved_by_segment');
            $table->date('date_of_contact');
            $table->string('region');
            $table->string('mbu_management_specialist');
            $table->string('result_telephone_contact_by_mbu');
            $table->integer('mpos_quantity_availables');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mercantil_clients');
    }
}
