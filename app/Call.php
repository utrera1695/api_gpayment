<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Call extends Model
{
    protected $table = 'calls';
    protected $fillable = ['date'];

    // Relaciones

    // Una llamada pertence a un cliente mercantil
    public function mercantilClients()
    {
    	return $this->belongsTo('App\MercantilClient');
    }

    // Una llamada pertence a un cliente mercantil no registrado
    public function unregisteredMercantilClients()
    {
    	return $this->belongsTo('App\UnregisteredMercantilClient');
    }

    // Métodos


    public static function whereMonth($month)
    {
        return DB::table('calls')->whereMonth('date', $month)->get();
    }

    public static function whereYear($year)
    {
        return DB::table('calls')->whereYear('date', $year)->get();
    }

    public static function charts()
    {
        $queryDateFormat = "DATE_FORMAT(date,'%b %Y') AS date_format";
        $queryMonth = "DATE_FORMAT(date,'%c') AS month";
        $queryYear = "DATE_FORMAT(date,'%Y') AS year";
        $calls = DB::table('calls')
                    ->select(DB::raw($queryDateFormat), DB::raw($queryMonth), DB::raw($queryYear))
                    ->orderBy('date', 'ASC')
                    ->distinct()->get();

        $charts = collect([]);
        foreach ($calls as $call) {
            // dd($call->month);
            $count = DB::table('calls')->whereMonth('date', $call->month)->whereYear('date', $call->year)->count();
            $charts->push(['date' => $call->date_format, 'count' => $count]);
        }
        return $charts;
    }

    public static function storeCallUnregistered($unregisteredMercantilClientId)
    {
        $call = new Call;
        $call->date = Carbon::now('America/Caracas');
        $call->unregistered_mercantil_client_id = $unregisteredMercantilClientId;
        $call->save();
    }
    
}
