<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp as Adapter;
use App\MercantilClient;
use Illuminate\Support\Facades\Storage;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['quotation_number', 'quotation_amount', 'date', 'order_number', 'seller', 'status', 'phase', 'stage', 'due_date', 'payment_confirmation_date', 'dispatch_date', 'received_client_date'];

    // Relaciones

    // Una orden tiene mucho documentos
    public function documents()
    {
    	return $this->hasMany('App\Document');
    }

    // Una orden es manejada por un usuario (agente)
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    // Una orden pertence a un cliente
    public function mercantilClient()
    {
        return $this->belongsTo('App\MercantilClient');
    }

    // Una orden puede ser modificada por muchas sesiones
    public function sessions()
    {
        return $this->hasMany('App\Session');
    }

    public static function uploadDocuments($request, $order)
    {
        $documentsName = [];
        // Ordem
        $path = $request->file('order')->store('documents_orders');
        array_push($documentsName, $path);

        // Cotización
        $path = $request->file('quotation')->store('documents_orders');
        array_push($documentsName, $path);
        return $documentsName;
    }
}
