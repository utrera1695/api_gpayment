<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use League\Flysystem\Filesystem;
use League\Flysystem\Adapter\Ftp as Adapter;

class Document extends Model
{
    protected $table = 'documents';
    protected $fillable = ['name', 'file', 'url'];

    // Relaciones

    // Muchos documentos pertenecen a una orden
    public function order()
    {
    	return $this->belongsTo('App\Order');
    }

    public static function uploadDocuments($documents, $order)
    {
    	$documentsName = [];
        foreach ($documents as $document) {
            $path = $document->store('documents_orders');
	        array_push($documentsName, [$path, $document->getClientOriginalName()]);
        }
        return $documentsName;
    }
}


