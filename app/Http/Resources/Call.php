<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\MercantilClient;

class Call extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'date' => $this->date,
            'mercantil_client_id' => MercantilClient::find($this->mercantil_client_id)->contact_person
        ];
    }
}
