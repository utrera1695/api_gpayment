<?php

namespace App\Http\Controllers;

use App\MercantilClient;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use File;

class MercantilClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mercantilClients = MercantilClient::all();
        return response()->json(['mercantilClients' => $mercantilClients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mercantilClient = new MercantilClient;

        $mercantilClient->commerce_name = $request->commerce_name;
        $mercantilClient->social_reason = $request->social_reason;
        $mercantilClient->identifier_data = $request->identifier_data;
        $mercantilClient->identification_number = $request->identification_number;
        $mercantilClient->contact_person = $request->contact_person;
        $mercantilClient->phone = $request->phone;
        $mercantilClient->email = $request->email;
        $mercantilClient->address = $request->address;
        // Other data
        $mercantilClient->approved_by_segment = "-";
        $mercantilClient->mpos_quantity = 0;
        $mercantilClient->date_of_contact = "-";
        $mercantilClient->region = "-";
        $mercantilClient->mbu_management_specialist = "-";
        $mercantilClient->result_telephone_contact_by_mbu = "-";
        $mercantilClient->mpos_quantity_availables = 1;
        $mercantilClient->save();
        return response()->json(['result' => 'El cliente fue creado exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MercantilClients  $mercantilClients
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mercantilClient = MercantilClient::find($id);
        return response()->json(['mercantilClient' => $mercantilClient]);
    }

    // Obtener un cliente de mercantil por el rif: &rif=
    public function getByRif(Request $request)
    {
        $rif = $request->get('rif');
        $mercantilClient = MercantilClient::where('identification_number', $rif)->first();
        return response()->json(['mercantilClient' => $mercantilClient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MercantilClients  $mercantilClients
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mercantilClient = MercantilClient::find($id);;
        $mercantilClient->commerce_name = $request->commerce_name;
        $mercantilClient->social_reason = $request->social_reason;
        $mercantilClient->identifier_data = $request->identifier_data;
        $mercantilClient->identification_number = $request->identification_number;
        $mercantilClient->contact_person = $request->contact_person;
        $mercantilClient->phone = $request->phone;
        $mercantilClient->email = $request->email;
        $mercantilClient->address = $request->address;
        // Other data
        if ($request->has('approved_by_segment')) {
           $mercantilClient->approved_by_segment = $request->approved_by_segment; 
        }

        if ($request->has('mpos_quantity')) {
           $mercantilClient->mpos_quantity = $request->mpos_quantity; 
        }

        if ($request->has('date_of_contact')) {
           $mercantilClient->date_of_contact = $request->date_of_contact; 
        }

        if ($request->has('region')) {
          $mercantilClient->region = $request->region;  
        }

        if ($request->has('mbu_management_specialist')) {
           $mercantilClient->mbu_management_specialist = $request->mbu_management_specialist; 
        }

        if ($request->has('result_telephone_contact_by_mbu')) {
           $mercantilClient->result_telephone_contact_by_mbu = $request->result_telephone_contact_by_mbu; 
        }

        if ($request->has('mpos_quantity_availables')) {
           $mercantilClient->mpos_quantity_availables = $request->mpos_quantity_availables; 
        }
        
        $mercantilClient->save();
        return response()->json(['result' => 'El cliente fue modificado exitosamente.']);
    }

    // Importar los datos del exceln a la BD
    public function import(Request $request)
    {
        $this->validate($request, [
            'excel' => 'required'
        ]);

        if ($request->hasFile('excel')) {
            $extension = File::extension($request->excel->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                $excel = Excel::load($request->excel->getRealPath(), function($reader) {
                    $results = $reader->ignoreEmpty();
                    $results = $reader->all();
                })->get()->toArray();
                
                foreach($excel as $row => $value) {
                    if (count($value) <80 && $row==0) {
                        for ($i=0; $i < 80; $i++) { 
                            if (!MercantilClient::where('identification_number', $value[$i]["nro_identificacion"])->first()) {
                            
                                $mercantilClient = new MercantilClient;
                                
                                $mercantilClient->commerce_name = $value[$i]["nombre_del_comercio"];
                                $mercantilClient->social_reason = $value[$i]["razon_social"];
                                $mercantilClient->identifier_data = $value[$i]["dato_identificador"];
                                $mercantilClient->identification_number = $value[$i]["nro_identificacion"];
                                $mercantilClient->contact_person = $value[$i]["nombre_persona_contacto"];
                                $mercantilClient->phone = $value[$i]["telefono"];
                                $mercantilClient->email = $value[$i]["correo_electronico"];
                                $mercantilClient->address = $value[$i]["direccion"];
                                // Other data
                                $mercantilClient->approved_by_segment = $value[$i]["piloto_aprobados_por_segmento"];
                                $mercantilClient->mpos_quantity = 0;
                                $mercantilClient->date_of_contact = $value[$i]["fecha_de_contacto"];
                                $mercantilClient->region = $value[$i]["region"];
                                $mercantilClient->mbu_management_specialist = $value[$i]["especialista_gestion_mbu"];
                                $mercantilClient->result_telephone_contact_by_mbu = $value[$i]["resultado_contacto_telefonico_por_mbu"];
                                $mercantilClient->mpos_quantity_availables = $value[$i]["cantidad_de_mpos"];
                                $mercantilClient->save();
                            }
                        }
                        
                    }else{
                        return response()->json(['result' => 'Para mayor eficiencia solo se aceptan archivos con un total de 80 registros o menos']);
                    }
                }
                return response()->json(['result' => 'Archivo importado exitosamente']);
            } else {
                return response()->json(['result' => 'Debe cargar un archivo con formato de excel.']);
            }            
        }            
    }

    // Exporta los datos
    public function export()
    {
        Excel::create('Datos de clientes', function($excel)  {
            $excel->sheet('Datos de clientes', function($sheet)  {
                $sheet->loadView('excel.mercantil-client', ['mercantilClients' => MercantilClient::export()]);
                return view('excel.mercantil-client', ['mercantilClients' => MercantilClient::export()]);
            });
        })->export('xlsx');
    }

    // Muestra todos los datos extra de un cliente mercantil
    public function getExtraData($id)
    {
        $mercantilClient = MercantilClient::find($id);
        return response()->json(['extraData' => $mercantilClient->extraData]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MercantilClients  $mercantilClients
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mercantilClient = MercantilClient::find($id);
        $mercantilClient->delete();
        return response()->json(['result' => 'El cliente fue removido exitosamente.']);
    }
}
