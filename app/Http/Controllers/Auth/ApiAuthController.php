<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use JWTAuth;
use JWTAuthException;
use App\User;

class ApiAuthController extends Controller
{

    public function __construct()
    {
        $this->user = new User;
    }

    public function login(Request $request)
    {

        $credentials = $request->only('email', 'password');

        $jwt = '';

        try {
            if (!$jwt = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'result' => 'error',
                    'message' => 'Credenciales incorrectas',
                ], 401);
            }
        } catch (JWTAuthException $e) {
            return response()->json([
                'result' => 'error',
                'message' => 'Falló la creación del token.',
            ], 500);
        }
        return response()->json(['token' => $jwt]);
    }

    public function getAuthUser(Request $request)
    {
        $token = $request->get('token');
        JWTAuth::setToken($token);
        $user = JWTAuth::parseToken()->authenticate();
        return response()->json(['user' => $user]);
    }
}