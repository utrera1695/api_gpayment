<?php

namespace App\Http\Controllers;

use App\Call;
use App\MercantilClient;
use App\Http\Requests\CallStore;
use App\Http\Resources\Call as CallResource;
use Illuminate\Http\Request;
use Carbon\Carbon;

use Illuminate\Support\Facades\DB;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CallResource::collection(Call::all());
    }

    // Retornar las llamadas de un día en concreto
    public function getByDay(Request $request)
    {
        $day = $request->get('day');
        $callByDay = Call::where('date', $day)->get();
        return response()->json(['calls' => $callByDay]);
    }

    // Retornar las llamadas de un mes en concretp
    public function getByMonth(Request $request)
    {
        $month = $request->get('month');
        $callByMonth= Call::whereMonth($month);
        return response()->json(['calls' => $callByMonth]);
    }

    // Retornar las llamadas de un año en concretp
    public function getByYear(Request $request)
    {
        $year = $request->get('year');
        $callByYear= Call::whereYear($year);
        return response()->json(['calls' => $callByYear]);
    }

    // Obtener las llamadas de un rif en concreto
    public function getByRif(Request $request)
    {
        $rif = $request->get('rif');
        $calls = MercantilClient::where('social_reason', $rif)->first();
        if ($calls) {
            return response()->json(['calls' => $calls->calls]);
        } else {
            return response()->json(['result' => 'Este cliente no tiene llamadas realizadas.']);
        }
    }

    // Retornar JSON con las estadísticas
    public function charts()
    {
        return response()->json(['charts' => Call::charts()]);;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CallStore $request)
    {
        $call = new Call;
        $call->date = Carbon::now('America/Caracas');
        $call->mercantil_client_id = $request->mercantil_client_id;
        $call->save();
        return response()->json(['result' => 'La llamada fue registrada exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $call = Call::find($id);
        return response()->json(['call' => $call]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $call = Call::find($id);;
        $call->date = $request->date;
        $call->mercantil_client_id = $request->mercantil_client_id;
        $call->unregistered_mercantil_client_id = $request->unregistered_mercantil_client_id;
        $call->save();
        return response()->json(['result' => 'La llamada fue actualizada exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Call  $call
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $call = Call::find($id);
        $call->delete();
        return response()->json(['result' => 'La llamada fue removida exitosamente.']);
    }
}
