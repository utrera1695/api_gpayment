<?php

namespace App\Http\Controllers;

use App\UnregisteredMercantilClient;
use App\Call;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UnregisteredMercantilClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $unregisteredMercantilClients = UnregisteredMercantilClient::all();
        return response()->json(['result' => $unregisteredMercantilClients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unregisteredMercantilClient = new UnregisteredMercantilClient;
        $unregisteredMercantilClient->commerce_name = $request->commerce_name;
        $unregisteredMercantilClient->person = $request->person;
        $unregisteredMercantilClient->social_reason = $request->social_reason;
        $unregisteredMercantilClient->contact_person = $request->contact_person;
        $unregisteredMercantilClient->phone = $request->phone;
        $unregisteredMercantilClient->email = $request->email;
        $unregisteredMercantilClient->region = $request->region;
        $unregisteredMercantilClient->observation = $request->observation;
        $unregisteredMercantilClient->save();
        return response()->json(['result' => 'El cliente no registrado ha sido creado exitosamente.']);
    }

    // Guardar y automáticamente aumentar una llamada
    public function storeWithCall(Request $request)
    {
        $unregisteredMercantilClient = new UnregisteredMercantilClient;
        $unregisteredMercantilClient->commerce_name = $request->commerce_name;
        $unregisteredMercantilClient->person = $request->person;
        $unregisteredMercantilClient->social_reason = $request->social_reason;
        $unregisteredMercantilClient->contact_person = $request->contact_person;
        $unregisteredMercantilClient->phone = $request->phone;
        $unregisteredMercantilClient->email = $request->email;
        $unregisteredMercantilClient->region = $request->region;
        $unregisteredMercantilClient->observation = $request->observation;
        $unregisteredMercantilClient->save();

        Call::storeCallUnregistered($unregisteredMercantilClient->id);

        return response()->json(['result' => 'El cliente ha sido registrado exitosamente y se ha registrado su primera llamada.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UnregisteredMercantilClient  $unregisteredMercantilClient
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unregisteredMercantilClient = UnregisteredMercantilClient::find($id);
        return response()->json(['unregisteredMercantilClient' => $unregisteredMercantilClient]);
    }

    // Exportar los datos de los no clientes
    public function export()
    {
        Excel::create('Lista de no registrados', function($excel)  {
            $excel->sheet('Lista de no registrados', function($sheet)  {
                $unregisteredMercantilClients = UnregisteredMercantilClient::all();
                $sheet->loadView('excel.unregistered-mercantil-clients', ['unregisteredMercantilClients' => $unregisteredMercantilClients]);
                return view('excel.unregistered-mercantil-clients', ['unregisteredMercantilClients' => $unregisteredMercantilClients]);
            });
        })->export('xlsx');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UnregisteredMercantilClient  $unregisteredMercantilClient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $unregisteredMercantilClient = UnregisteredMercantilClient::find($id);;
        $unregisteredMercantilClient->commerce_name = $request->commerce_name;
        $unregisteredMercantilClient->person = $request->person;
        $unregisteredMercantilClient->social_reason = $request->social_reason;
        $unregisteredMercantilClient->contact_person = $request->contact_person;
        $unregisteredMercantilClient->phone = $request->phone;
        $unregisteredMercantilClient->email = $request->email;
        $unregisteredMercantilClient->region = $request->region;
        $unregisteredMercantilClient->observation = $request->observation;
        $unregisteredMercantilClient->save();
        return response()->json(['result' => 'El cliente no registrado ha sido actualizado exitosamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UnregisteredMercantilClient  $unregisteredMercantilClient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unregisteredMercantilClient = UnregisteredMercantilClient::find($id);
        $unregisteredMercantilClient->delete();
        return response()->json(['result' => 'El cliente no registrado ha sido removido exitosamente.']);
    }
}
