<?php

namespace App\Http\Controllers;

use App\Order;
use App\MercantilClient;
use App\Session;
use App\Notifications\OrderCreate;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\SendOrderDocuments;
use JWTAuth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return response()->json(['orders' => $orders]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        date_default_timezone_set('America/Caracas');
        $mercantilClient = MercantilClient::find($request->mercantil_client_id);
        if ($mercantilClient->mpos_quantity_availables) {
            $date = Carbon::now('America/Caracas');
            $order = new Order;
            $random = getDate();
            $random = 'D' . $random['mday'] . 'M' . $random['mon'] . 'Y' .$random['year'] . '|V:' . $request->seller;
            $order->quotation_number = '0';//'COT:' . $random . '|MC:' . $mercantilClient->identification_number . '|R:' . strtoupper(str_random(5));
            $order->date = $date->toDateTimeString();
            $order->order_number = '0';//'ORD:' . $random . '|MC:' . $mercantilClient->identification_number . '|R:' . strtoupper(str_random(5));
            $order->seller = $request->seller;
            $order->status = 'valid';
            $order->phase = 'Call Center';
            $order->stage = 'Orden creada';
            $order->due_date = $date->addDay()->toDateTimeString();

            $order->mercantil_client_id = $request->mercantil_client_id;
            $order->quotation_amount = $request->quotation_amount;
            
            $order->save();

            // $mercantilClient->mpos_quantity_availables--;
            // $mercantilClient->mpos_quantity++;
            /* $mercantilClient->notify(new OrderCreate());
             */// $mercantilClient->save();

            // Guardar session
            //Session::store($order, JWTAuth::parseToken()->authenticate()->id, 0);
            return response()->json(['result' => 'La orden fue creada exitosamente.']);
        } else {
            return response()->json(['result' => 'El cliente ya exedió el límite de compras.']);
        }
    }

    // Enviar los documentos de la orden (número de orden y número de cotización)
    public function sendOrderDocuments(Request $request, $id)
    {
        $order = Order::find($id);
        $documentsName = Order::uploadDocuments($request, $order);
        $order->mercantilClient->notify(new SendOrderDocuments($documentsName));

        // Session
        //Session::store($order, JWTAuth::parseToken()->authenticate()->id, 1);

        return response()->json(['result' => 'Los documentos han enviados exitosamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('mercantil_client_id', $id)->first();
        return response()->json(['order' => $order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $order = Order::find($id);
        $order->quotation_number = $request->quotation_number;
        $order->order_number = $request->order_number;
        $order->stage = $request->stage;
        $order->phase = $request->phase;

        $order->save();
        return response()->json(['result' => 'La orden fue modificada exitosamente.']);
    }

    // Función para cambiar el estado a una orden de compra
    public function changeStatus($id)
    {
        $order = Order::find($id);
        $order->status = ($order->status == 'enable') ? 'disable' : 'enable';
        $order->save();

        //Session::store($order, JWTAuth::parseToken()->authenticate()->id, 3);

        return response()->json(['result' => 'La orden fue actualizada exitosamente.']);
    }

    // Función para cambiar la fase de una orden de compra
    public function changePhase(Request $request, $id)
    {
       $order = Order::find($id);
        $order->phase = $request->phase;
        $order->save();

        //Session::store($order, JWTAuth::parseToken()->authenticate()->id, 2);

        return response()->json(['result' => 'La orden fue actualizada exitosamente.']); 
    }

    // Actualiza la fehca de despacho
    public function updateDispatchDate(Request $request, $id)
    {
        $order = Order::find($id);
        $order->dispatch_date = $request->dispatch_date;
        $order->save();
        return response()->json(['result' => 'La fecha de despacho fue actualizada correctamente.']);
    }

    // Actualiza la fecha de recepción por el cliente
    public function updateReceivedClientDate(Request $request, $id)
    {
        $order = Order::find($id);
        $order->received_client_date = $request->received_client_date;
        $order->save();
        return response()->json(['result' => 'La fecha de recepción por el cliente fue actualizada correctamente.']);
    }

    // Ver los documentos de una orden de compra
    public function documents($id)
    {
        $documents = Order::find($id)->documents;
        return response()->json(['documents' => $documents]);
    }

    // Check el traking de una orden de compra
    public function traking($id)
    {
        $order = Order::find($id);
        return response()->json(['order' => ['phase' => $order->phase, 'stage' => $order->stage]]);
    }

    // Ver la fase en que está una orden de compra
    public function phase(Request $request)
    {
        $orders = Order::where('phase', $request->get('phase'))->get();
        return response()->json(['orders' => $orders]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $order = Order::find($id);

       // Session::store($order, JWTAuth::parseToken()->authenticate()->id, 4);
        $order->delete();
        
        return response()->json(['result' => 'La orden fue removida exitosamente.']);
    }
}
