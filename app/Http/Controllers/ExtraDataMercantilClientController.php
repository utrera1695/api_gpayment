<?php

namespace App\Http\Controllers;

use App\ExtraDataMercantilClient;
use Illuminate\Http\Request;
use App\Http\Resources\ExtraDataMercantilClient as ExtraDataMercantilClientResource;

class ExtraDataMercantilClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ExtraDataMercantilClientResource::collection(ExtraDataMercantilClient::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $extraDataMercantilClient = new ExtraDataMercantilClient;
        $extraDataMercantilClient->attribute = $request->attribute;
        $extraDataMercantilClient->value = $request->value;
        $extraDataMercantilClient->mercantil_client_id = $request->mercantil_client_id;
        $extraDataMercantilClient->save();
        return response()->json(['result' => 'Se ha guardado el dato extra correctamente.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExtraDataMercantilClient  $extraDataMercantilClient
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $extraDataMercantilClient = ExtraDataMercantilClient::find($id);
        return response()->json(['extra_data' => $extraDataMercantilClient]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExtraDataMercantilClient  $extraDataMercantilClient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $extraDataMercantilClient = ExtraDataMercantilClient::find($id);
        $extraDataMercantilClient->attribute = $request->attribute;
        $extraDataMercantilClient->value = $request->value;
        $extraDataMercantilClient->mercantil_client_id = $request->mercantil_client_id;
        $extraDataMercantilClient->save();
        return response()->json(['result' => 'Se ha actualizado el dato extra correctamente.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExtraDataMercantilClient  $extraDataMercantilClient
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $extraDataMercantilClient = ExtraDataMercantilClient::find($id);
        $extraDataMercantilClient->delete();
    }
}
