<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UnregisteredMercantilClientStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'commerce_name' => 'required',
            'person' => 'required',
            'social_reason' => 'required',
            'contact_person' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'region' => 'required',
            'observation' => 'required',
        ];
    }
}
