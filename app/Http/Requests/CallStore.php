<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CallStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mercantil_client_id' => 'nullable|exists:mercantil_clients,id',
            'unregistered_mercantil_client_id' => 'nullable|exists:unregistered_mercantil_clients,id',
        ];
    }
}
