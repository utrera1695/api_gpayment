<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MercantilClientStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'commerce_name' => 'required',
            'person' => 'required',
            'social_reason' => 'required',
            'identifier_data' => 'required',
            'identification_number' => 'required',
            'contact_person' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
            'approved_by_segment' => 'required',
            'mpos_quantity' => 'required',
            'date_of_contact' => 'required|date',
            'region' => 'required',
            'observation' => 'required',
            'mpos_quantity_availables' => 'required',
        ];
    }
}
