<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class Session extends Model
{
    protected $table = 'sessions';
    protected $fillable = ['action', 'date_action'];

    // Relaciones

    // Una sesion pertenece a un usuario
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    // Una sesion puede tener una orden modificada
    public function order()
    {
    	return $this->belongsTo('App\Order');
    }

    // Métodos

    public static function store($order, $userId, $messageId)
    {
        $date = Carbon::now('America/Caracas');
        $user = User::find($userId);
        $user = $user->name . ' ' . $user->surname;
        $session = new Session;
        $session->action = getMessage($user, $order, $order->mercantilClient->contact_person, $messageId);
        $session->date_action = $date->toDateTimeString();
        $session->user_id = $userId;
        $session->order_id = $order->id;
        $session->save();
    }
}
