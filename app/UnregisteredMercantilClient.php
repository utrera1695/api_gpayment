<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnregisteredMercantilClient extends Model
{
    protected $table = 'unregistered_mercantil_clients';
    protected $fillable = ['commerce_name', 'person', 'social_reason', 'contact_person', 'phone', 'email', 'region', 'observation'];

    // Relaciones
    
    // Un cliente puede realizar muchas llamadas
    public function calls()
    {
    	return $this->hasMany('App\Call');
    }
}
