<?php

	function formatterEmail($email)
	{
		$email = str_replace(' ', '_', $email);
	 
	    $email = str_replace(
	        array('á', 'à', 'ä', 'â', 'ã', 'ª', 'Á', 'À', 'Â', 'Ä'),
	        array('a', 'a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
	        $email
    	);
 
	    $email = str_replace(
	        array('é', 'è', 'ë', 'ê', 'ẽ', 'É', 'È', 'Ê', 'Ë'),
	        array('e', 'e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
	        $email
	    );
	 
	    $email = str_replace(
	        array('í', 'ì', 'ï', 'î', 'ĩ', 'Í', 'Ì', 'Ï', 'Î'),
	        array('i', 'i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
	        $email
	    );
	 
	    $email = str_replace(
	        array('ó', 'ò', 'ö', 'ô', 'õ', 'Ó', 'Ò', 'Ö', 'Ô'),
	        array('o', 'o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
	        $email
	    );
	 
	    $email = str_replace(
	        array('ú', 'ù', 'ü', 'û', 'ũ', 'Ú', 'Ù', 'Û', 'Ü'),
	        array('u', 'u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
	        $email
    	);
	 
	    $email = str_replace(
	        array('ñ', 'ç'),
	        array('n', 'c'),
	        $email
	    );
	 
	    $email = str_replace(
	        array("\\", "¨", "º", "-", "~",
	             "#", "|", "!", "\"",
	             "·", "$", "%", "&", "/",
	             "(", ")", "?", "'", "¡", '"',
	             "¿", "[", "^", "<code>", "]",
	             "+", "}", "{", "¨", "´",
	             ">", "< ", ";", ",", ":", ""),
	        ' ',
	        $email
	    );
		return strtolower($email);
	}

	function getMessage($user, $order, $mercantilClient, $message)
	{
		$messages = [
			'ha creado la orden al usuario ',
			'ha enviando los documentos de la orden del usuario ',
			'ha cambiado la fase de la orden del usuario',
			'ha cambiado el estatus la orden del usuario',
			'ha eliminado la orden del usuario'
		];
		return "El usuario $user $messages[$message] $mercantilClient con el número de cotización $order->quotation_number bajo el número $order->order_number";
	}