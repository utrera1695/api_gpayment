<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class MercantilClient extends Model
{
    use Notifiable;
    protected $table = 'mercantil_clients';
    protected $fillable = ['commerce_name', 'social_reason', 'identifier_data', 'identification_number', 'contact_person', 'phone', 'email', 'address', 'mpos_quantity','approved_by_segment', 'date_of_contact', 'region', 'mbu_management_specialist', 'result_telephone_contact_by_mbu', 'mpos_quantity_availables'];

    // Relaciones

    // Un cliente puede realizar muchas ordenes
    public function order()
    {
    	return $this->hasMany('App\Order');
    }

    // Un cliente puede realizar muchas llamadas
    public function calls()
    {
    	return $this->hasMany('App\Call');
    }

    // Un cliente puede tener muchos datos extras
    public function extraData()
    {
        return $this->hasMany('App\ExtraDataMercantilClient');
    }

    // Mètodos

    public static function export()
    {
        return  DB::table('mercantil_clients')
                    ->join('orders', 'mercantil_clients.id', 'orders.mercantil_client_id')
                    ->get();
    }
}
