<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtraDataMercantilClient extends Model
{
    protected $table = 'extra_data_mercantil_clients';
    protected $fillable = ['attribute', 'value'];

    // Relaciones

    // Una dato extra pertenece a un cliente
    public function mercantilClient()
    {
    	return $this->belongsTo('App\MercantilClient');
    }
}
